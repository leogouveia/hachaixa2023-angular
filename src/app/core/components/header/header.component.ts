import { Component } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
  acionaMenu: boolean = false;
  arrayMenu: Array<any> = [
    {
      nome: 'Botões',
      icone: '',
      filhos: [
        {
          nome: 'Item 1',
          icone: 'menu',
          callBack: () => {
            console.log('Ação');
          },
        },
      ],
    },
  ];
  constructor() {}

  buttonAcion() {
    this.acionaMenu = !this.acionaMenu;
  }
}
