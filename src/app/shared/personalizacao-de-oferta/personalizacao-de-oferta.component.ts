import { Component, Input, OnInit } from '@angular/core';
import { Produto } from '../shared/models/produto.model';

@Component({
  selector: 'app-personalizacao-de-oferta',
  templateUrl: './personalizacao-de-oferta.component.html',
  styleUrls: ['./personalizacao-de-oferta.component.scss'],
})
export class PersonalizacaoDeOfertaComponent implements OnInit {
  @Input() produtos: Produto[] = [];
  personalizaSimulacaoChecked: boolean = false;

  ngOnInit(): void {}

  togglePersonalizaSimulacao() {
    this.personalizaSimulacaoChecked = !this.personalizaSimulacaoChecked;
  }
}
