import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonalizacaoDeOfertaComponent } from './personalizacao-de-oferta.component';

describe('PersonalizacaoDeOfertaComponent', () => {
  let component: PersonalizacaoDeOfertaComponent;
  let fixture: ComponentFixture<PersonalizacaoDeOfertaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PersonalizacaoDeOfertaComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PersonalizacaoDeOfertaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
