import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, catchError, throwError } from 'rxjs';
import { Produto } from '../models/produto.model';
import { environment } from 'src/environments/environment';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root',
})
export class ParametrosContratoService {
  private readonly API = environment.apiUrl;
  private httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };

  constructor(private httpClient: HttpClient, private snackbar: MatSnackBar) {}

  listarProdutosDisponiveisParaOfertas(): Observable<Produto[]> {
    return this.httpClient
      .get<Produto[]>(
        `${this.API}/api/v1/parametros-contrato`,
        this.httpOptions
      )
      .pipe(catchError((err) => this.handleError(err)));
  }

  handleError(error: HttpErrorResponse | any) {
    console.log(error);
    let customError =
      'Houve um erro ao pesquisar os produtos. Tente novamente mais tarde.';
    if (error.status === 404) {
      customError = 'Nenhum produto encontrado.';
    } else if (error.message) {
      customError = error.message;
    }

    this.snackbar.open(`Erro: ${customError}`, 'OK', {
      duration: 100000,
      verticalPosition: 'top',
      horizontalPosition: 'right',
      politeness: 'assertive',
      panelClass: ['snackbar-error'],
    });
    return throwError(() => error);
  }
}
