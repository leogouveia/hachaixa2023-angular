import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

const API: string =
  'https://49c9-200-166-198-2.ngrok-free.app/api/v1/parametros-contrato';

@Injectable({
  providedIn: 'root'
})
export class SimularContratoService {

  private httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };

  constructor(private httpClient: HttpClient) {}

  simular(dadosParaSimulacao:JSON) {
    return this.httpClient.post<any>(API, dadosParaSimulacao, this.httpOptions).pipe();
  }

}
