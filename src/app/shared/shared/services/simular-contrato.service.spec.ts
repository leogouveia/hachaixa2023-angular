/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { SimularContratoService } from './simular-contrato.service';

describe('Service: SimularContrato', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SimularContratoService]
    });
  });

  it('should ...', inject([SimularContratoService], (service: SimularContratoService) => {
    expect(service).toBeTruthy();
  }));
});
