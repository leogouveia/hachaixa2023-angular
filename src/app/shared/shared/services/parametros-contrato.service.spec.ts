/* tslint:disable:no-unused-variable */

import { TestBed, inject } from '@angular/core/testing';
import { ParametrosContratoService } from './parametros-contrato.service';

describe('Service: ParametrosContrato', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ParametrosContratoService],
    });
  });

  it('should ...', inject(
    [ParametrosContratoService],
    (service: ParametrosContratoService) => {
      expect(service).toBeTruthy();
    }
  ));
});
