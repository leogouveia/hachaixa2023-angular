export interface Produto {

  nuProduto: number;
  noProduto: string;
  dtInicioVigencia: Date;
  dtFimVigencia: Date;
  pcTaxaMulta: number;
  pcTaxaMora: number;
  icTipoPrazo: string;
  taxa: number;

}
