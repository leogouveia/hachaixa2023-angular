import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonalizacaoDeOfertaCriacaoComponent } from './personalizacao-de-oferta-criacao.component';

describe('PersonalizacaoDeOfertaCriacaoComponent', () => {
  let component: PersonalizacaoDeOfertaCriacaoComponent;
  let fixture: ComponentFixture<PersonalizacaoDeOfertaCriacaoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PersonalizacaoDeOfertaCriacaoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PersonalizacaoDeOfertaCriacaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
