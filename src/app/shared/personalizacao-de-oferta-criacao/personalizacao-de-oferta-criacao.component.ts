import { OfertaPersonalizada } from './../shared/models/oferta-personalizada';
import { SimularContratoService } from './../shared/services/simular-contrato.service';
import { Component, Input, OnInit } from '@angular/core';
import { Form, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Produto } from '../shared/models/produto.model';

@Component({
  selector: 'app-personalizacao-de-oferta-criacao',
  templateUrl: './personalizacao-de-oferta-criacao.component.html',
  styleUrls: ['./personalizacao-de-oferta-criacao.component.scss'],
})
export class PersonalizacaoDeOfertaCriacaoComponent implements OnInit {
  @Input() produtos: Produto[] = [];
  form!: FormGroup;
  ofertaPersonalizada!:OfertaPersonalizada;
  permissaoCedida: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private simularContratoService: SimularContratoService
  ) {}

  ngOnInit(): void {
    this.criarFormulario();
  }

  criarFormulario() {
    this.form = this.formBuilder.group({
      cpf: [],
      nuProduto: [],
      valor: [],
      prazo: [],
    });
  }

  simular() {
    if (this.form.valid) {
      const valueSumit = Object.assign(this.form.value, JSON);
      this.simularContratoService.simular(valueSumit)
      .subscribe(response=>{
        this.ofertaPersonalizada = response
      });
    }
  }
}
