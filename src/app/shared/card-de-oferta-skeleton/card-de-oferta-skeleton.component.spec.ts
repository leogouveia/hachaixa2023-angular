import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardDeOfertaSkeletonComponent } from './card-de-oferta-skeleton.component';

describe('CardDeOfertaSkeletonComponent', () => {
  let component: CardDeOfertaSkeletonComponent;
  let fixture: ComponentFixture<CardDeOfertaSkeletonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardDeOfertaSkeletonComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CardDeOfertaSkeletonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
