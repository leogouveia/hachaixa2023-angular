import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'app-card-de-oferta-skeleton',
  templateUrl: './card-de-oferta-skeleton.component.html',
  styleUrls: ['./card-de-oferta-skeleton.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CardDeOfertaSkeletonComponent {

}
