import { Produto } from './../shared/models/produto.model';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-card-de-oferta',
  templateUrl: './card-de-oferta.component.html',
  styleUrls: ['./card-de-oferta.component.scss'],
})
export class CardDeOfertaComponent {
  @Input() produto!: Produto;
  @Input() img!: string | number;
}
