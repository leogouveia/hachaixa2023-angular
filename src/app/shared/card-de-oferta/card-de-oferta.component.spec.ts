import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardDeOfertaComponent } from './card-de-oferta.component';

describe('CardDeOfertaComponent', () => {
  let component: CardDeOfertaComponent;
  let fixture: ComponentFixture<CardDeOfertaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardDeOfertaComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CardDeOfertaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
