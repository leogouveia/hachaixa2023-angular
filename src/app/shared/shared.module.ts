import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardDeOfertaComponent } from './card-de-oferta/card-de-oferta.component';
import { OfertaPersonalizadaComponent } from './oferta-personalizada/oferta-personalizada.component';
import { PersonalizacaoDeOfertaComponent } from './personalizacao-de-oferta/personalizacao-de-oferta.component';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';

import { HttpClientModule } from '@angular/common/http';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { PersonalizacaoDeOfertaCriacaoComponent } from './personalizacao-de-oferta-criacao/personalizacao-de-oferta-criacao.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatButtonModule } from '@angular/material/button';
import { CardDeOfertaSkeletonComponent } from './card-de-oferta-skeleton/card-de-oferta-skeleton.component';

@NgModule({
  declarations: [
    CardDeOfertaComponent,
    OfertaPersonalizadaComponent,
    PersonalizacaoDeOfertaComponent,
    PersonalizacaoDeOfertaCriacaoComponent,
    CardDeOfertaSkeletonComponent,
  ],
  imports: [
    HttpClientModule,
    CommonModule,
    MatCardModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatSlideToggleModule,
    MatInputModule,
    MatSelectModule,
    MatCheckboxModule,
    MatButtonModule,
  ],
  exports: [
    CardDeOfertaComponent,
    OfertaPersonalizadaComponent,
    PersonalizacaoDeOfertaComponent,
    PersonalizacaoDeOfertaCriacaoComponent,
  ],
})
export class SharedModule {}
