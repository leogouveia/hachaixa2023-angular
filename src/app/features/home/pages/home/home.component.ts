import { ParametrosContratoService } from 'src/app/shared/shared/services/parametros-contrato.service';
import {
  AfterViewChecked,
  Component,
  ElementRef,
  OnInit,
  ViewChild,
} from '@angular/core';
import { Produto } from 'src/app/shared/shared/models/produto.model';

@Component({
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit, AfterViewChecked {
  durationnSeconds = 1;
  existeOfertaPersonalizada = false;
  produtos: Produto[] = [];
  skeletonOverflowed = false;
  private _skeleton: ElementRef | undefined;
  @ViewChild('skeleton') set skeleton(content: ElementRef | undefined) {
    if (content) {
      this._skeleton = content;
    }
  }

  constructor(private parametrosContratoService: ParametrosContratoService) {}

  ngOnInit(): void {
    this.parametrosContratoService
      .listarProdutosDisponiveisParaOfertas()
      .subscribe((response) => (this.produtos = response));
    this.checkOverflow();
  }

  ngAfterViewChecked(): void {
    this.checkOverflow();
  }

  checkOverflow() {
    const element = this._skeleton?.nativeElement;
    if (
      element.scrollHeight > element.clientHeight ||
      element.scrollWidth > element.clientWidth
    ) {
      this.skeletonOverflowed = true;
    } else {
      this.skeletonOverflowed = false;
    }
  }
}
