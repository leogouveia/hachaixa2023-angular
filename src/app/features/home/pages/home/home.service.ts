import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject, catchError, of, tap } from 'rxjs';

import { Produto } from 'src/app/shared/shared/models/produto.model';
import { ParametrosContratoService } from 'src/app/shared/shared/services/parametros-contrato.service';

@Injectable({
  providedIn: 'root',
})
export class HomeService {
  private status = new BehaviorSubject<'loading' | 'error' | 'success'>(
    'loading'
  );
  status$ = this.status.asObservable();

  constructor(
    private parametrosProdutosService: ParametrosContratoService,
    private snackBar: MatSnackBar
  ) {}

  getProdutos() {
    return this.parametrosProdutosService
      .listarProdutosDisponiveisParaOfertas()
      .pipe(
        catchError(() => {
          this.status.next('error');
          return [];
        }),
        tap(() => this.status.next('success'))
      );
  }
}
